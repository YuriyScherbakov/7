﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Advanced
{
    class FileExplorer
    {
        public bool CreateTree(TreeView treeView)
        {
            bool returnValue = false;

            try
            {

                foreach ( DriveInfo drv in DriveInfo.GetDrives() )
                {

                    TreeNode fChild = new TreeNode();
                    fChild.Text = drv.Name;
                    fChild.Nodes.Add("");
                    treeView.Nodes.Add(fChild);
                    returnValue = true;
                }

            }
            catch ( Exception ex )
            {
                returnValue = false;
            }
            return returnValue;

        }
        public TreeNode EnumerateDirectory(TreeNode parentNode,TextBox tb)
        {
            DirectoryInfo rootDir = new DirectoryInfo(parentNode.FullPath);

            try
            {

                parentNode.Nodes [0].Remove();
                foreach ( DirectoryInfo dir in rootDir.GetDirectories() )
                {

                    TreeNode node = new TreeNode();
                    node.Text = dir.Name;
                    node.Nodes.Add("");
                    parentNode.Nodes.Add(node);
                }
            }

            catch ( Exception ex )
            {
                tb.Text = ex.Message;
            }

            return parentNode;
        }
    }
}
