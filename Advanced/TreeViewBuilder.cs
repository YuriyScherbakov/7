﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Advanced
{
    class TreeViewBuilder
    {
        static private int currentLineToReed;
        public static string path;
        public static object locker = new object();

        public static bool treeViewBuilderStoppedFlag = true;


        public delegate void BuiltNewTreeNodeEventHandler(object source,TreeNodeArgs treeNodeArgs);
        public event BuiltNewTreeNodeEventHandler BuiltNewTreeNode;

        public delegate void ErrorOccuredTreeViewBuilderEventHandler(object source,ErrorOccuredArgs errorOccuredArgs);
        public event ErrorOccuredTreeViewBuilderEventHandler ErrorOccuredTreeViewBuilder;

        protected virtual void OnErrorOccuredTreeViewBuilder(ErrorOccuredArgs errorOccuredArgs)
        {

            if ( ErrorOccuredTreeViewBuilder != null )
            {

                ErrorOccuredTreeViewBuilder(this,errorOccuredArgs);
            }

        }

        protected virtual void OnBuiltNewTreeNode(TreeNodeArgs treeNodeArgs)
        {

            if ( BuiltNewTreeNode != null )
            {

                BuiltNewTreeNode(this,treeNodeArgs);
            }

        }

        public void FillTreeNode()
        {
            TreeNode treeNode = new TreeNode();
            bool isFirstNode = true;
            string sizeStr;

            while ( true )
            {
               

                if ( treeViewBuilderStoppedFlag )
                {
                    treeNode = null;

                    return;
                }

                TreeNode currentNode;

                Link link = LinksKeeper.GetLink(currentLineToReed);

                if ( LinksKeeper.LinksCount() > currentLineToReed )
                {
                    if ( link == null)
                    {
                        continue;
                    }

                    var path = link.FullName;
                    bool isFile = link.isFile;
                    string creationDate = link.CreationDate;
                    long size = link.size;
                    string driveName = LinksKeeper.driveName;
                    long sizeDrive = LinksKeeper.sizeDrive;



                    currentNode = treeNode;
                    foreach ( string subLink in path.Split('\\') )
                    {
                        if ( currentNode.Nodes [subLink] == null )
                        {
                            if ( isFirstNode )
                            {
                                currentNode = currentNode.Nodes.Add(subLink, "Диск " + subLink + ".  Размер " + sizeDrive);

                                isFirstNode = false;

                            }
                            else
                            {
                                if ( size == -1 )
                                {
                                    sizeStr = "неизвестен - доступ запрещен";
                                }
                                else
                                {
                                    sizeStr = size.ToString();
                                }

                                if ( link.isFile )
                                {
                                    currentNode = currentNode.Nodes.Add(subLink,"Файл " + subLink + ".  Размер " + sizeStr + ".  " + "Дата создания " + creationDate);
                                }
                                else
                                {
                                    currentNode = currentNode.Nodes.Add(subLink,"Папка " + subLink + ". Размер " + " " + sizeStr + ".  " + "Дата создания " + creationDate);
                                }
                            }

                        }
                        else
                        {
                            currentNode = currentNode.Nodes [subLink];
                        }
                    }
                    currentLineToReed++;
                }

                bool discCrawlerStoppedFlag = DiscCrawler.discCrawlerStoppedFlag;
                if ( currentLineToReed == LinksKeeper.LinksCount() && discCrawlerStoppedFlag )
                {
                    break;
                }

            }

            TreeNode tn = treeNode.FirstNode;
            TreeNodeArgs treeNodeEventArgs = new TreeNodeArgs() { treenode = tn };

            try
            {
                if ( tn == null )
                {
                    throw new Exception("После построения TreeNode, его значение null.");
                }

                OnBuiltNewTreeNode(treeNodeEventArgs);

            }
            catch ( Exception e )
            {

                ErrorOccuredArgs err = new ErrorOccuredArgs() { e = e };
                OnErrorOccuredTreeViewBuilder(err);

            }




            treeNode = null;


        }
        public void TreeViewBuilderController()
        {
            while ( true )
            {
                if ( treeViewBuilderStoppedFlag == false )
                {




                    if ( LinksKeeper.LinksCount() < 1 )
                    {
                        continue;
                    }
                    FillTreeNode();
                    currentLineToReed = 0;
                    treeViewBuilderStoppedFlag = true;
                }



            }
        }


    }
}

