﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced
{
   public class Link
    {
        public bool isFile
        {
            set; get;
           
        }

        string fullName;

        public string FullName
        {
            set
            {
                if ( value == null )
                {
                    fullName = "";
                }
                else
                {
                    fullName = value;
                }

            }
            get
            {
                return fullName;
            }
            
        }
        public long size
        {
            set; get;
        }

        string creationDate;

        public string CreationDate
        {
            set
            {
                if ( value == null )
                {
                    creationDate = "";
                }
                else
                {
                    creationDate = value;
                }
            }
            get
            {
                return creationDate;
            }
        }

    }
}
