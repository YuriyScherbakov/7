﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Advanced
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            this.buttonAbort.Enabled = false;

        }
        Info info;
        string path;
        FileExplorer fe = new FileExplorer();

        bool xmlWriterFinished = false;
        bool treeViewBuilderFinished = false;


        void buttonsStateReturn()
        {
            Action buttonsStateReturn = () =>
            {
                this.buttonStart.Enabled = true;
                this.buttonAbort.Enabled = false;

            };

            if ( treeViewBuilderFinished & xmlWriterFinished )
            {
                Invoke(buttonsStateReturn);
            }

        }

        public void ShowSavedXMLpath(object sender, XMLReadyArgs e)
        {
            Action message = () =>
            {
                MessageBox.Show("Файл XML создан." + System.Environment.NewLine +
                    "Полное имя файла " + e.xmlPath,
           "XML Writer Message",
           MessageBoxButtons.OK,
           MessageBoxIcon.Information,
           MessageBoxDefaultButton.Button3);
            };


            Invoke(message);
            xmlWriterFinished = true;

            buttonsStateReturn();



        }
        public void DrawTreeView(object sender,TreeNodeArgs e)
        {

            Action action = () =>
            {
                this.treeView2.Nodes.Clear();
                this.treeView2.Nodes.Add(e.treenode);

            };


            Invoke(action);
            treeViewBuilderFinished = true;

            buttonsStateReturn();

        }
        public void XMLSavedMessage(object sender,TreeNodeArgs e)
        {

            Action message = () =>
            {
                MessageBox.Show("Файл XML создан." + System.Environment.NewLine +
                    "Имя файла " + "",
           "XML Writer Message",
           MessageBoxButtons.YesNo,
           MessageBoxIcon.Warning,
           MessageBoxDefaultButton.Button2);
            };


            Invoke(message);
           

        }
       
        private void StartSearching()
        {
            xmlWriterFinished = false;
            treeViewBuilderFinished = false;
            this.buttonStart.Enabled = false;
            this.buttonAbort.Enabled = true;

            this.treeView2.Nodes.Clear();

            Monitor.Enter(DiscCrawler.locker);
            DiscCrawler.path = path;// DiscCrawler видит path и начинает поиск
            Monitor.Exit(DiscCrawler.locker);

            Monitor.Enter(XMLWriter.locker);
            XMLWriter.xmlWriterStoppedFlag = false;// XMLWriter видит false и начинает чтение списка
            Monitor.Exit(XMLWriter.locker);

            Monitor.Enter(TreeViewBuilder.locker);
            TreeViewBuilder.treeViewBuilderStoppedFlag = false;//TreeViewBuilder видит false и начинает строить дерево
            Monitor.Exit(TreeViewBuilder.locker);

        }
        private void button1_Click(object sender,EventArgs e)
        {
            if ( this.path == null )
            {
                DialogResult result = MessageBox.Show("Пожалуйста, выберите директорию.",
           "Директория не выбрана",
           MessageBoxButtons.OK,
           MessageBoxIcon.Warning,
           MessageBoxDefaultButton.Button2);

               
                    return;
               
              
            }

            if ( this.path.Count() < 4 )
            {
                DialogResult result = MessageBox.Show("При выборе всего диска, процесс обработки может занять длительное время (десятки минут). Продолжить?",
           "Выбранный диск " + path,
           MessageBoxButtons.YesNo,
           MessageBoxIcon.Warning,
           MessageBoxDefaultButton.Button2);

                if ( result == DialogResult.Yes )
                {
                    StartSearching();
                }
                else
                    return;
            }

            StartSearching();


        }
        private void button2_Click(object sender,EventArgs e)
        {
            this.buttonStart.Enabled = true;
            this.buttonAbort.Enabled = false;

            Monitor.Enter(DiscCrawler.locker);
            DiscCrawler.path = null;
            Monitor.Exit(DiscCrawler.locker);

            Monitor.Enter(XMLWriter.locker);
            XMLWriter.xmlWriterStoppedFlag = true;// XMLWriter видит true и останавливается
            Monitor.Exit(XMLWriter.locker);

            Monitor.Enter(TreeViewBuilder.locker);
            TreeViewBuilder.treeViewBuilderStoppedFlag = true;// TreeViewBuilder видит true и останавливается
            Monitor.Exit(TreeViewBuilder.locker);

        }

        private void treeView1_BeforeExpand(object sender,TreeViewCancelEventArgs e)
        {
            if ( e.Node.Nodes [0].Text == "" )
            {
                fe.EnumerateDirectory(e.Node,this.textBox1);
            }
        }

        private void Form1_Load(object sender,EventArgs e)
        {
            fe.CreateTree(this.treeView1);

        }

        private void treeView1_AfterSelect(object sender,TreeViewEventArgs e)
        {
            path = e.Node.FullPath;

            if ( path.Count() > 4 )
            {
                path = e.Node.FullPath.Remove(2,1);
            }

        }

       
        public void ErrorsWriter(object sender, ErrorOccuredArgs e )
        {
            treeViewBuilderFinished = true;
            this.textBox1.WriteToTextBox(e.e.Message + " " + sender +" " + e.e.Source);

            Action buttonsStateReturn = () =>
            {
                this.buttonStart.Enabled = true;
                this.buttonAbort.Enabled = false;
              
            };


            Invoke(buttonsStateReturn);

           
        }

        private void button1_Click_1(object sender,EventArgs e)
        {
            if ( info != null )
            {
                info.Close();
                info = new Info();
                info.Show();
                
                return;
            }
            info = new Info();
            info.Show();
        }

        private void MainForm_FormClosing(object sender,FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }

}
