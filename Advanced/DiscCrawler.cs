﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Advanced
{
    class DiscCrawler
    {
        public static string path;
        public static bool discCrawlerStoppedFlag;

        public static object locker = new object();

        public delegate void ErrorOccuredDiscCrawlerEventHandler(object source,ErrorOccuredArgs errorOccuredArgs);
        public event ErrorOccuredDiscCrawlerEventHandler ErrorOccuredDiscCrawler;

        protected virtual void OnErrorOccuredDiscCrawler(ErrorOccuredArgs errorOccuredArgs)
        {

            if ( ErrorOccuredDiscCrawler != null )
            {

                ErrorOccuredDiscCrawler(this,errorOccuredArgs);
            }

        }

        public long SizeOfDrive(string driveName)
        {
            foreach ( DriveInfo driveInfo in DriveInfo.GetDrives() )
            {
                if ( driveInfo.Name == driveName )
                {
                    return driveInfo.TotalSize;
                }
            }
            return 0;
        }
        private long SizeOfDirectory(DirectoryInfo dirInfo)
        {
            long size = 0;
            FileInfo [] files;
            try
            {

                files = dirInfo.GetFiles("*.*",SearchOption.AllDirectories);
            }
            catch
            {
                return -1;
            }

            foreach ( var file in files )
            {
                size += file.Length;
            }

            return size;

        }
        private void TreeViewKeeperFiller(DirectoryInfo dirInfo)
        {
            if ( path == null )
            {//Если пользователь нажал сброс
                LinksKeeper.LinksClear();
                DiscCrawlerController();
            }
            LinksKeeper.AddNewLink(
                            new Link()
                            {
                                isFile = false,
                                CreationDate = dirInfo.CreationTime.ToShortDateString(),
                                FullName = dirInfo.FullName,
                                size = SizeOfDirectory(dirInfo)
                            });

            DirectoryInfo [] dirInfoArr;
            try
            {
                dirInfoArr = dirInfo.GetDirectories();
            }
            catch
            {
                return;
            }
           
            foreach ( var dir in dirInfoArr )
            {
                
                LinksKeeper.AddNewLink(
                            new Link()
                            {
                                isFile = false,
                                CreationDate = dir.CreationTime.ToShortDateString(),
                                FullName = dir.FullName,
                                size = SizeOfDirectory(dir)
                            });
                TreeViewKeeperFiller(dir);
                
            }
            foreach ( var file in dirInfo.GetFiles() )
            {
                try
                {

                    LinksKeeper.AddNewLink(
                        new Link()
                        {
                            isFile = true,
                            CreationDate = file.CreationTime.ToShortDateString(),
                            FullName = file.FullName,
                            size = file.Length
                        });
                }
                catch ( PathTooLongException pathTooLong )
                {

                }

            }


        }

        private void PathForTreeViewKeeperFiller(string path)
        {
            string driveName = LinksKeeper.driveName = path.Substring(0,3);

           LinksKeeper.sizeDrive = SizeOfDrive(driveName);
            discCrawlerStoppedFlag = false;            
            TreeViewKeeperFiller(new DirectoryInfo(path));

            if ( LinksKeeper.LinksCount() == 0 )
            {//Если указана пустая папка
              
                LinksKeeper.AddNewLink(new Link()
                {
                    FullName = path,
                    CreationDate = "nothingIsCreatedHere"
                });
            }
            discCrawlerStoppedFlag = true;

        }

        public void DiscCrawlerController()
        {
            while ( true )
            {
                if ( path != null )
                {
                    LinksKeeper.LinksClear();
                    Monitor.Enter(XMLWriter.locker);
                    XMLWriter.pathForXMLfile = path;
                    Monitor.Exit(XMLWriter.locker);


                    try
                    {
                        //Thread.Sleep(3);
                        //int.Parse("QWERTY");
                        ////Для умышленной генерации ошибки

                        PathForTreeViewKeeperFiller(path);
                    }
                    catch ( Exception e )
                    {

                        Monitor.Enter(DiscCrawler.locker);
                        DiscCrawler.path = null;
                        Monitor.Exit(DiscCrawler.locker);

                        Monitor.Enter(XMLWriter.locker);
                        XMLWriter.xmlWriterStoppedFlag = true;// XMLWriter видит true и останавливается
                        Monitor.Exit(XMLWriter.locker);

                        Monitor.Enter(TreeViewBuilder.locker);
                        TreeViewBuilder.treeViewBuilderStoppedFlag = true;// TreeViewBuilder видит true и останавливается
                        Monitor.Exit(TreeViewBuilder.locker);

                        ErrorOccuredArgs err = new ErrorOccuredArgs() { e = e };

                        OnErrorOccuredDiscCrawler(err);
                    }

                    path = null;
                }

                discCrawlerStoppedFlag = true;

            }
        }
    }
}
