﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Advanced
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

           

            DiscCrawler discCrawler = new DiscCrawler();
            Thread discCrawlerThread = new Thread(discCrawler.DiscCrawlerController);
            discCrawlerThread.Name = "discCrawler";
            discCrawlerThread.IsBackground = true;
            discCrawlerThread.Start();

            XMLWriter xmlWriter = new XMLWriter();
            Thread xmlWriterThread = new Thread(xmlWriter.XMLWriterController);
            xmlWriterThread.Name = "xmlWriterThread";
            xmlWriterThread.IsBackground = true;
            xmlWriterThread.Start();

            TreeViewBuilder treeViewBuilder = new TreeViewBuilder();
            Thread treeViewBuilderThread = new Thread(treeViewBuilder.TreeViewBuilderController);
            treeViewBuilderThread.Name = "treeViewBuilderThread";
            treeViewBuilderThread.IsBackground = true;
            treeViewBuilderThread.Start();

            MainForm mainForm = new MainForm();
            treeViewBuilder.BuiltNewTreeNode += mainForm.DrawTreeView;
            xmlWriter.SavedNewXMLfile += mainForm.ShowSavedXMLpath;

            discCrawler.ErrorOccuredDiscCrawler += mainForm.ErrorsWriter;
            xmlWriter.ErrorOccuredXMLWriter += mainForm.ErrorsWriter;
            treeViewBuilder.ErrorOccuredTreeViewBuilder += mainForm.ErrorsWriter;

            Application.Run(mainForm);
        }
    }
}
