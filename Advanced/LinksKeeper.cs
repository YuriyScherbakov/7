﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Advanced
{
    class LinksKeeper
    {
      
        static List<Link> linksObj = new List<Link>();
        public static string driveName;
        public static long sizeDrive;

        static readonly object locker = new object();

        static void SetLock()
        {
            Monitor.Enter(locker);

        }
        static void UnsetLock()
        {
            Monitor.Exit(locker);
        }
        static public void AddNewLink(Link link)
        {
            SetLock();
            LinksKeeper.linksObj.Add(link);
            UnsetLock();
        }
        static public int LinksCount()
        {
            SetLock();
            int count = linksObj.Count;
            UnsetLock();
            return count;
        }
        static public void LinksClear()
        {
            SetLock();
            linksObj.Clear();
            UnsetLock();
           
        }

        static public Link GetLink(int currentLink)
        {
            Link link;
            SetLock();
            if ( LinksKeeper.linksObj.Count() > currentLink )
            {
                link = LinksKeeper.linksObj.ElementAt(currentLink);
            }
            else
            {
                link = null;
            }

            UnsetLock();
            return link;
        }

    }
}
