﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Advanced
{
    public delegate void AddErroeMessageDelegate(TextBox textBox, string message);

   static class  TextBoxExtention
    {
       
        public static void WriteToTextBox(this ISynchronizeInvoke textBox, string message)
        {
            AddErroeMessageDelegate addMessage = AddErrorMessage;

            if ( textBox is TextBox )
            {
                ( textBox as TextBox ).Invoke(addMessage,textBox, message);

            }
          
            
        }
        public static void AddErrorMessage(TextBox textBox, string message)
        {
            textBox.Text += Environment.NewLine;
            textBox.Text += Environment.NewLine;
            textBox.Text += message;
        }
    }
}
