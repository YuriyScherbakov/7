﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace Advanced
{
    class XMLWriter
    {


        private int currentLineToReed;

        XDocument xDocument = new XDocument();
        XElement xElement = new XElement("DIR");
        public static string pathForXMLfile = "XML.xml";

        public static bool xmlWriterStoppedFlag = true;

        public static object locker = new object();


        public delegate void SavedNewXMLfileEventHandler(object source,XMLReadyArgs xmlReadyArgs);
        public event SavedNewXMLfileEventHandler SavedNewXMLfile;

        public delegate void ErrorOccuredXMLWriterEventHandler(object source,ErrorOccuredArgs errorOccuredArgs);
        public event ErrorOccuredXMLWriterEventHandler ErrorOccuredXMLWriter;

        protected virtual void OnErrorOccuredXMLWriter(ErrorOccuredArgs errorOccuredArgs)
        {

            if ( ErrorOccuredXMLWriter != null )
            {

                ErrorOccuredXMLWriter(this,errorOccuredArgs);
            }

        }

        protected virtual void OnSavedNewXMLfile(XMLReadyArgs xmlReadyArgs)
        {

            if ( SavedNewXMLfile != null )
            {

                SavedNewXMLfile(this,xmlReadyArgs);
            }

        }


        public void XMLWriterController()
        {
            while ( true )
            {
                if ( xmlWriterStoppedFlag == false )
                {
                    xDocument = new XDocument();
                    xElement = new XElement("DIR");

                    if ( LinksKeeper.LinksCount() < 1 )
                    {
                        continue;
                    }
                    XmlMaker();

                    currentLineToReed = 0;
                    xmlWriterStoppedFlag = true;
                }
            }
        }

        public void XmlMaker()
        {

            const string node = "folder";
            const string label = "name";
            bool isFirstNode = true;

            while ( true )
            {
                if ( xmlWriterStoppedFlag )
                {
                    xElement = null;
                    xDocument = null;
                    return;
                }
                Link link = LinksKeeper.GetLink(currentLineToReed);

                if ( link != null )
                {
                    var labelValues = link.FullName.Split('\\');
                    var currentNode = xElement;

                    foreach ( var labelValue in labelValues )
                    {
                        var foundNode = currentNode.Elements(node).Where
                            (n => (string)n.Attribute(label) == labelValue).SingleOrDefault();

                        if ( foundNode != null )
                        {
                            currentNode = foundNode;
                        }
                        else
                        {
                            var newNode = new XElement(node,new XAttribute(label,labelValue));
                            if ( link.isFile )
                            {
                                newNode.Add(( new XAttribute("file","true") ));
                                newNode.Add(( new XAttribute("size",link.size + " bytes") ));
                                newNode.Add(( new XAttribute("creationDate",link.CreationDate) ));
                            }
                            else
                            {
                                if ( isFirstNode )
                                {
                                    newNode.Add(( new XAttribute("file","true") ));
                                    if ( LinksKeeper.sizeDrive == 0 )
                                    {
                                        newNode.Add(( new XAttribute("totalSize","unknown") ));


                                    }
                                    else
                                    {
                                        newNode.Add(( new XAttribute("totalSize",LinksKeeper.sizeDrive.ToString() + " bytes") ));

                                    }
                                    isFirstNode = false;
                                }
                                else
                                {
                                    newNode.Add(( new XAttribute("file","false") ));
                                    if ( link.size == -1 )
                                    {
                                        newNode.Add(( new XAttribute("size","unknown access denied") ));


                                    }
                                    else
                                    {
                                        newNode.Add(( new XAttribute("size",link.size.ToString() + " bytes") ));

                                    }
                                    newNode.Add(( new XAttribute("creationDate",link.CreationDate) ));

                                }
                            }
                            currentNode.Add(newNode);
                            currentNode = newNode;
                        }
                    }
                    currentLineToReed++;
                }

                bool discCrawlerStoppedFlag = DiscCrawler.discCrawlerStoppedFlag;


                if ( currentLineToReed == LinksKeeper.LinksCount() && discCrawlerStoppedFlag )
                {
                    break;
                }

            }
            try
            {
                RefineXDocument(xElement);
                xDocument.Add(xElement.Nodes());

                xDocument.Root.Name = "LocalDisc";

                SaveXDocument(xDocument);

                xElement = null;
                xDocument = null;


            }
            catch ( Exception e )
            {

                ErrorOccuredArgs err = new ErrorOccuredArgs() { e = e };

                OnErrorOccuredXMLWriter(err);
            }




        }
        private void SaveXDocument(XDocument xDocument)
        {
            string path = pathForXMLfile;

            if ( path.Count() < 5 )
            {
                path = path.FirstOrDefault().ToString();
            }
            else
            {
                foreach ( var item in path.Split('\\') )
                {
                    path = item;
                }

            }
            try
            {
                xDocument.Save(path + ".xml");


                XMLReadyArgs xmlReadyArgs = new XMLReadyArgs()
                {
                    xmlPath = Environment.CurrentDirectory
                + "\\" + path + ".xml"
                };

                OnSavedNewXMLfile(xmlReadyArgs);
            }
            catch ( Exception )
            {

                throw;
            }

        }

        void RefineXDocument(XElement xElement)
        {
            var v = xElement.Descendants().Where(q => q.Name.LocalName == "folder");
            foreach ( var node in v )
            {
                try
                {
                    if ( node.Attribute("file").Value == "true" )
                    {
                        node.Name = "file";
                    }
                }
                catch
                {
                    continue;
                }

                RefineXDocument(node);
                try
                {
                    node.Attribute("file").Remove();

                }
                catch
                {

                }
            }



        }


    }
}
